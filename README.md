# README #

1/3スケールのSHARP X-1マニアタイプ風小物（NEW version）のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1982年11月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/X1_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF))
- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/e/eeadd8511a119bc76596799087fd02d4)
- [懐かしのパソコン](https://greendeepforest.com/?p=2401)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_x1mania_new/raw/1b8dcd917cd0d21ddf563611b5adc0b954ef2524/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1mania_new/raw/1b8dcd917cd0d21ddf563611b5adc0b954ef2524/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1mania_new/raw/1b8dcd917cd0d21ddf563611b5adc0b954ef2524/ExampleImage.jpg)
